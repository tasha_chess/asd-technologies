import React from 'react';
import { Form, Input, Button, Alert, Card, Typography } from 'antd';

import { withAuthContext } from '../../context/AuthContext'

import styles from './styles.module.scss';

const { Title } = Typography;

const AuthForm = ({ authProps: { authorization, error, isFetching } }) => (
    <>
        <Title level={3}>Log in</Title>
        <Card className={styles.card}>
            <Form
                labelCol={{ span: 24 }}
                onFinish={authorization}
            >
                <Form.Item label="Username" required name="username">
                    <Input placeholder="Name" disabled={isFetching} />
                </Form.Item>
                <Form.Item label="Password" name="password" required>
                    <Input type="password" placeholder="Password" disabled={isFetching} />
                </Form.Item>
                {error && <Alert className={styles.error} message={error} type="error" />}
                <Button htmlType="submit" type="primary" loading={isFetching} className={styles.button}>
                    Login
                </Button>
            </Form>
        </Card>
    </>
);

export default withAuthContext(AuthForm);
