import React from 'react';
import {Col, Row} from 'antd';

import { withAuthContext } from '../../context/AuthContext';

import styles from './styles.module.scss';
import DataComponent from '../DataComponent';
import AuthForm from '../AuthForm';

const Main = ({ authProps: { isAuth, setAuth, logout } }) => {
  return (
      <div className={styles.wrapper}>
          <Row className={styles.content}>
              <Col xs={{ span: 18, offset: 3 }} lg={{ span: 6, offset: 9 }}>
                  {isAuth ? <DataComponent setAuth={setAuth} /> : <AuthForm />}
              </Col>
          </Row>
      </div>
  )
};

export default withAuthContext(Main)
