import React from 'react';
import { Button, Spin, Card } from 'antd';
import { LoadingOutlined, ApiFilled, DisconnectOutlined } from '@ant-design/icons';

const Template = ({ isConnected, time, logout }) => {
    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;
    const title = isConnected ? 'Connected' : 'Disconnected';
    const statusIcon = isConnected ?
        <ApiFilled style={{ color: 'green', fontSize: 24 }} /> :
        <DisconnectOutlined style={{ color: 'red', fontSize: 24 }} />;

    return (
        <>
            <Card title={title} extra={statusIcon}>
                {time && isConnected ? time : <Spin indicator={antIcon} />}
            </Card>
            <Button style={{ marginTop: '16px' }} onClick={logout}>Log out</Button>
        </>
    );
}

export default Template;
