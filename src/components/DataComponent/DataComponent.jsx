import React, { useState, useEffect } from 'react';
import axios from 'axios';
import moment from 'moment';

import Template from './Template';
import { openNotification } from '../Notification';

import { API, TOKEN_KEY } from '../../constants';

let socket;
let interval;

const DataComponent = ({ setAuth }) => {
    const [isConnected, setConnected] = useState(false);
    const [time, setTime] = useState('');
    const token = window.localStorage.getItem(TOKEN_KEY);


    const connect = () => axios.get(API.SUBSCRIBE, {
        headers: { 'x-test-app-jwt-token': token }
    })
        .then((result) => openWs(result.data))
        .catch((error) => {
            if (error.status === 401) {
                logout();
                return false;
            }

            openNotification({
                    message: `Error ${error?.response?.status}` || 'Error',
                    description: error.data?.description || 'Unknown error'
            });
        });

    const effect = () => {
        connect();

        interval = setInterval(() => {
            if(socket && socket.readyState !== 1) {
                connect();
            }
        }, 2000);

        return () => {
            clearInterval(interval);
            socket = null;
        }
    };

    useEffect(effect, []);

    const logout = () => {
        clearInterval(interval);
        window.localStorage.removeItem(TOKEN_KEY);
        socket = null;
        setAuth(false);
    }

    const openWs = ({ url }) => {
        socket = new WebSocket(url);

        socket.onopen = () => {
            setConnected(true);
        }

        socket.onclose = () => {
            setConnected(false);
            connect();
        }

        socket.onmessage = function(event) {
            setTime(moment(event.data?.server_time).format('DD.MM.YYYY HH:mm:ss'));
        };

        socket.onerror = function() {
            logout();
            setConnected(false);
        };
    }

    return <Template isConnected={isConnected} time={time} logout={logout} />;
};

export default DataComponent;
