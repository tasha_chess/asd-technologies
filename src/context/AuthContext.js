import React, { createContext, useState } from 'react';
import axios from 'axios';

import { API, TOKEN_KEY } from '../constants';
import { withConsumerGenerator } from './utils';

export const Context = createContext({});

export const AuthContext = ({ children }) => {
    const [error, setError] = useState(null);
    const [isFetching, setFetching] = useState(false);
    const [isAuth, setAuth] = useState(!!window.localStorage.getItem(TOKEN_KEY));

    const logout = () => {
        window.localStorage.removeItem(TOKEN_KEY);
        setAuth(false);
    };

    const authorization = (values) => {
        setError(null);
        let attemptCount = 0;

        const login = () => {
            setFetching(true);
            axios.post(API.LOGIN, values)
                .then(res => {
                    window.localStorage.setItem('token', res?.headers['x-test-app-jwt-token']);
                    setAuth(true);
                    setError(null);
                    setFetching(false)
                })
                .catch((error) => {
                    if ((error.response.status !== 400 && error.response.status !== 401) && attemptCount < 3) {
                        login();
                        attemptCount++;
                        return false;
                    }

                    if (error.response) {
                        setError(error.response?.data?.description || 'Unknown error');
                        setFetching(false);
                    }
                })
        }

        login();
    };

    return <Context.Provider value={{
        isAuth, authorization, error, setAuth, isFetching, logout
    }}>{children}</Context.Provider>
}

export const withAuthContext = withConsumerGenerator(Context.Consumer, 'authProps')
