import React from 'react';

export const withConsumerGenerator = (Consumer, propName) => {
    return (Component) => {
        return (ownProps) => {
            const render = (consumerProps) => {
                const props = {
                    ...ownProps,
                    [propName]: consumerProps,
                };

                return <Component {...props} />;
            };

            return (
                <Consumer>
                    {render}
                </Consumer>
            );
        };
    };
};
