const API_URL = 'https://work.vint-x.net/api';

export const API = {
    LOGIN: `${API_URL}/login`,
    SUBSCRIBE: `${API_URL}/subscribe`
}

export const TOKEN_KEY = 'token';
