import React from 'react';

import { AuthContext } from './context/AuthContext';
import Main from './components/Main';

function App() {
  return (
      <AuthContext>
        <Main />
      </AuthContext>
  );
}

export default App;
